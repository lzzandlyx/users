package main

import (
	"context"
	"flag"
	"fmt"
	"gitee.com/lzzandlyx/users/db/migrate"

	"gitee.com/lzzandlyx/users/internal/config"
	addr "gitee.com/lzzandlyx/users/internal/server/address"
	users "gitee.com/lzzandlyx/users/internal/server/user"
	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	flag.Parse()

	var c config.Config
	conf.MustLoad(*config.ConfigFile, &c)
	ctx := svc.NewServiceContext(c)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		user.RegisterUserServer(grpcServer, users.NewUserServer(ctx))
		user.RegisterAddressServer(grpcServer, addr.NewAddressServer(ctx))

		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})
	defer s.Stop()

	err := migrate.AutoMigrate(context.Background())
	if err != nil {
		panic(err)
	}

	fmt.Printf("Starting rpc server at %s...\n", c.ListenOn)
	s.Start()
}
