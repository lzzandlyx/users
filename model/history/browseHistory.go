package history

import (
	"context"
	"gitee.com/lzzandlyx/users/db/mysql"
	"gorm.io/gorm"
)

// BrowseHistory 浏览记录表
type BrowseHistory struct {
	gorm.Model
	UserId     int64  `json:"user_id"      gorm:"column:user_id;type:int(11);comment:用户ID"`
	GoodsId    int64  `json:"goods_id"     gorm:"column:goods_id;type:int(11);comment:商品id"`
	Title      string `json:"title"        gorm:"column:title;type:varchar(30);comment:商品名称"`
	Price      string `json:"price"        gorm:"column:price;type:decimal(10,2);comment:商品价格"`
	Cover      string `json:"cover"        gorm:"column:cover;type:varchar(255);comment:商品图片"`
	BrowseDate string `json:"browse_date"  gorm:"column:browse_date;type:varchar(255);comment:浏览日期"`
}

func NewHistory() *BrowseHistory {
	return &BrowseHistory{}
}
func (b *BrowseHistory) CreateHistory(ctx context.Context, in *BrowseHistory) (*BrowseHistory, error) {
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Create(&in).Error
	})
	return in, err
}
