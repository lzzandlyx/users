package user

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/lzzandlyx/users/db/mysql"
	"gorm.io/gorm"
	"time"
)

// User 用户表
type User struct {
	gorm.Model
	Mobile      string `json:"mobile"         gorm:"column:mobile;type:char(11);unique;comment:手机号"`
	Password    string `json:"password"       gorm:"column:password;type:varchar(50);comment:密码"`
	Nickname    string `json:"nickname"       gorm:"column:nickname;type:varchar(30);comment:昵称"`
	Sex         int64  `json:"sex"            gorm:"column:sex;type:tinyint(1);comment:性别 0-未知, 1-男, 2-女"`
	Birthday    string `json:"birthday"       gorm:"column:birthday;type:varchar(255);comment:出生日期"`
	Email       string `json:"email"          gorm:"column:email;type:varchar(255);comment:邮箱"`
	Avatar      string `json:"avatar"         gorm:"column:avatar;type:varchar(255);comment:头像"`
	Salt        string `json:"salt"           gorm:"column:salt;type:varchar(255);comment:加盐"`
	Openid      string `json:"openid"         gorm:"column:openid;type:varchar(50);comment:开放ID"`
	Unionid     string `json:"unionid"        gorm:"column:unionid;type:varchar(50);comment:微信登录唯一标识"`
	State       int64  `json:"state"          gorm:"column:state;type:tinyint(1);comment:状态 0封禁 1 正常 "`
	LoginSource string `json:"login_source"   gorm:"column:login_source;type:varchar(10);comment:登录来源"`
	Points      int64  `json:"points"         gorm:"column:points;type:int(11);comment:用户积分"`
}

// PointRecord
type PointRecord struct {
	gorm.Model
	UserId        int64     `json:"user_id"        gorm:"column:user_id;type:int(11);index;comment:用户ID"`
	ChangeType    string    `json:"change_type"    gorm:"column:change_type;type:varchar(30);comment:积分变动类型"`
	Amount        int64     `json:"amount"         gorm:"column:amount;type:int(11);comment:积分变动数量"`
	Balance       int64     `json:"balance"        gorm:"column:balance;type:int(11);comment:操作后的积分余额"`
	OperationTime time.Time `json:"operation_time" gorm:"column:operation_time;type:datetime;comment:操作时间"`
	Description   string    `json:"description"    gorm:"column:description;type:varchar(50);comment:积分操作描述"`
	Source        string    `json:"source"         gorm:"column:source;type:varchar(30);comment:积分来源"`
	OrderID       int64     `json:"order_id"       gorm:"column:order_id;type:int(11);comment:订单来源"`
}

func NewUser() *User {
	return &User{}
}

func NewPointRecord() *PointRecord {
	return &PointRecord{}
}

// CreatePointsRecord 创建积分变动记录
func (p *PointRecord) CreatePointsRecord(ctx context.Context, in *PointRecord) (*User, error) {
	var user User
	//使用事务处理,
	err := mysql.WithTxClient(ctx, func(tx *gorm.DB) error {
		//查询用户信息
		if err := tx.First(&user, in.UserId).Error; err != nil {
			return err //用户不存在,返回错误
		}
		//更新用户积分
		user.Points += in.Amount
		if err := tx.Save(&user).Error; err != nil {
			return err
		}

		pointRecord := PointRecord{
			UserId:        in.UserId,
			ChangeType:    in.ChangeType,
			Amount:        in.Amount,
			Balance:       user.Points,
			OperationTime: time.Now(),
			Description:   in.Description,
			Source:        in.Source,
			OrderID:       in.OrderID,
		}
		//事务创建积分记录
		if err := tx.Create(&pointRecord).Error; err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (u *User) GetUserInfoByMobile(ctx context.Context, mobile string) (*User, error) {
	var in User
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Where("mobile = ?", mobile).Find(&in).Error
	})

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, fmt.Errorf("user with mobile %s not found", mobile)
		}
		return nil, err
	}

	return &in, err
}

func (u *User) Register(ctx context.Context, in *User) (*User, error) {
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Create(&in).Error
	})
	return in, err
}

func (u *User) GetUserInfoByOpenID(ctx context.Context, openId string) (*User, error) {
	var in User
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Where("openid = ?", openId).Find(&in).Error
	})

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, fmt.Errorf("user with openId %s not found", openId)
		}
		return nil, err
	}

	return &in, err
}

func (u *User) Delete(ctx context.Context, id int64) error {
	return mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Where("id = ?", id).Delete(&u).Error
	})
}

func (u *User) Update(ctx context.Context, id int64, updateInfo *User) error {
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Model(&User{}).Where("id = ?", id).Updates(&updateInfo).Error
	})

	return err
}

func (u *User) UpdatePwd(ctx context.Context, mobile, password string) error {
	return mysql.WithClient(ctx, func(db *gorm.DB) error {
		err := db.Model(&User{}).Where("mobile = ?", mobile).Update("password", password).Error

		return err
	})
}

func (u *User) UpdateMobile(ctx context.Context, id int64, mobile string) error {
	return mysql.WithClient(ctx, func(db *gorm.DB) error {
		err := db.Model(&User{}).Where("id = ?", id).Update("mobile", mobile).Error

		return err
	})
}

func (u *User) GetUserInfoByID(ctx context.Context, id int64) (*User, error) {
	var user User
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Where("id = ?", id).First(&user).Error // 直接使用 First
	})

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, fmt.Errorf("user with id %d not found", id)
		}
		return nil, err
	}

	return &user, nil
}
