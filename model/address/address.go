package address

import (
	"context"
	"errors"
	"gitee.com/lzzandlyx/users/db/mysql"
	"gorm.io/gorm"
)

// Addresses 地址表
type Addresses struct {
	gorm.Model
	UserId    int64  `json:"user_id"      gorm:"column:user_id;type:int(11);comment:用户ID"`
	Name      string `json:"name"         gorm:"column:name;type:varchar(30);comment:收货人姓名"`
	Mobile    string `json:"mobile"       gorm:"column:mobile;type:char(11);comment:手机号"`
	Province  string `json:"province"     gorm:"column:province;type:varchar(20);comment:省"`
	City      string `json:"city"         gorm:"column:city;type:varchar(30);comment:市"`
	County    string `json:"county"       gorm:"column:county;type:varchar(30);comment:县"`
	Township  string `json:"township"     gorm:"column:township;type:varchar(30);comment:乡镇"`
	Address   string `json:"address"      gorm:"column:address;type:varchar(200);comment:详细地址"`
	IsDefault bool   `json:"is_default"   gorm:"column:is_default;type:tinyint(1);comment:是否默认地址 true 是 false 否"`
}

func NewAddress() *Addresses {
	return &Addresses{}
}

func (a *Addresses) CreateAdd(ctx context.Context, in *Addresses) (*Addresses, error) {
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Create(&in).Error
	})
	return in, err
}

func (a *Addresses) DeleteAddress(ctx context.Context, id int64) error {
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Where("id = ?", id).Delete(&a).Error
	})

	return err
}

func (a *Addresses) UpdateAddress(ctx context.Context, id int64, updateInfo *Addresses) error {
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Model(&Addresses{}).Where("id = ?", id).Updates(*updateInfo).Error
	})
	return err
}

func (a *Addresses) GetAddressInfoByID(ctx context.Context, id int64) (*Addresses, error) {
	var info *Addresses
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Where("id = ?", id).Find(&info).Limit(1).Error
	})

	return info, err
}

func (a *Addresses) AddressList(ctx context.Context) ([]Addresses, error) {
	var addresses []Addresses

	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Find(&addresses).Limit(1).Error
	})

	if err != nil {
		return nil, err
	}

	return addresses, err
}

func (a *Addresses) UpdateDefaultAddress(ctx context.Context, userId int64, id int64) error {
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		if err := db.Model(&Addresses{}).Where("user_id = ?", userId).Update("is_default", false).Error; err != nil {
			return err
		}
		return db.Model(&Addresses{}).Where("id = ?", id).Update("is_default", true).Error
	})
	return err
}

func (a *Addresses) GetDefaultAddr(ctx context.Context, userId int64) (*Addresses, error) {
	var info Addresses
	err := mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.Where("user_id = ? AND is_default = ?", userId, true).Find(&info).Limit(1).Error
	})

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, err
		}
		return nil, err
	}
	return &info, nil
}
