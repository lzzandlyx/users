package migrate

import (
	"context"
	"gitee.com/lzzandlyx/users/db/mysql"
	"gitee.com/lzzandlyx/users/model/address"
	"gitee.com/lzzandlyx/users/model/history"
	"gitee.com/lzzandlyx/users/model/user"
	"gorm.io/gorm"
)

func AutoMigrate(ctx context.Context) error {
	return mysql.WithClient(ctx, func(db *gorm.DB) error {
		return db.AutoMigrate(
			&user.User{},
			&address.Addresses{},
			&history.BrowseHistory{},
			&user.PointRecord{},
		)
	})
}
