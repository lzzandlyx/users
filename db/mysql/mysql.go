package mysql

import (
	"context"
	"fmt"
	"gitee.com/lzzandlyx/users/internal/config"
	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/logc"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func WithClient(ctx context.Context, handler func(db *gorm.DB) error) error {
	var c config.Config
	conf.MustLoad(*config.ConfigFile, &c)

	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local",
		c.Mysql.User,
		c.Mysql.Password,
		c.Mysql.Host,
		c.Mysql.Port,
		c.Mysql.DBName,
	)
	cli, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		logc.Error(ctx, err)
		return err
	}

	db, err := cli.DB()
	if err != nil {
		logc.Error(ctx, err)
		return err
	}

	defer db.Close()

	return handler(cli)
}

func WithTxClient(ctx context.Context, handler func(tx *gorm.DB) error) error {
	return WithClient(ctx, func(db *gorm.DB) error {
		var err error
		tx := db.Begin()

		defer func() {
			if err == nil {
				tx.Commit()
			} else {
				tx.Rollback()
			}
		}()

		return handler(tx)
	})
}
