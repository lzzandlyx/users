package userlogic

import (
	"context"
	"fmt"
	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"
	"github.com/jinzhu/copier"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetUserInfoLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserInfoLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserInfoLogic {
	return &GetUserInfoLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetUserInfoLogic) GetUserInfo(in *user.GetUserInfoRequest) (*user.GetUserInfoResponse, error) {
	if in.ID == 0 {
		return nil, fmt.Errorf("ID 不能为空")
	}

	info, err := l.svcCtx.ModelUser.GetUserInfoByID(l.ctx, in.ID)
	if err != nil {
		return nil, err
	}

	var resInfo user.UserInfo
	err = copier.Copy(&resInfo, &info)
	if err != nil {
		return nil, err
	}

	return &user.GetUserInfoResponse{
		Info: &resInfo,
	}, nil
}
