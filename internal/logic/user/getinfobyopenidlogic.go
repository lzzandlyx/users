package userlogic

import (
	"context"
	"fmt"
	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"
	"github.com/jinzhu/copier"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetInfoByOpenIDLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetInfoByOpenIDLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetInfoByOpenIDLogic {
	return &GetInfoByOpenIDLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetInfoByOpenIDLogic) GetInfoByOpenID(in *user.GetInfoByOpenIDRequest) (*user.GetInfoByOpenIDResponse, error) {
	if in.OpenId == "" {
		return nil, fmt.Errorf("OPENID 不能为空")
	}

	info, err := l.svcCtx.ModelUser.GetUserInfoByOpenID(l.ctx, in.OpenId)
	if err != nil {
		return nil, err
	}

	var resInfo user.UserInfo
	err = copier.Copy(&resInfo, &info)
	if err != nil {
		return nil, err
	}

	return &user.GetInfoByOpenIDResponse{
		Info: &resInfo,
	}, nil
}
