package userlogic

import (
	"context"
	"gitee.com/lzzandlyx/users/internal/svc"
	user1 "gitee.com/lzzandlyx/users/model/user"
	"gitee.com/lzzandlyx/users/user"
	"github.com/jinzhu/copier"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateLogic {
	return &UpdateLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateLogic) Update(in *user.UpdateRequest) (*user.UpdateResponse, error) {
	u := user1.User{}

	err := copier.Copy(&u, &in.Info)
	if err != nil {
		return nil, err
	}

	err = l.svcCtx.ModelUser.Update(l.ctx, in.ID, &u)
	if err != nil {
		return nil, err
	}

	return &user.UpdateResponse{}, nil
}
