package userlogic

import (
	"context"
	"fmt"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateMobileLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateMobileLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateMobileLogic {
	return &UpdateMobileLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateMobileLogic) UpdateMobile(in *user.UpdateMobileRequest) (*user.UpdateMobileResponse, error) {
	if in.ID == 0 {
		return nil, fmt.Errorf("ID不能为为空")
	}
	if in.Mobile == "" {
		return nil, fmt.Errorf("手机号不能为空")
	}

	err := l.svcCtx.ModelUser.UpdateMobile(l.ctx, in.ID, in.Mobile)
	if err != nil {
		return nil, err
	}

	return &user.UpdateMobileResponse{}, nil
}
