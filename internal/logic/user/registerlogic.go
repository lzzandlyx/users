package userlogic

import (
	"context"
	"fmt"
	"gitee.com/lzzandlyx/users/internal/svc"
	user1 "gitee.com/lzzandlyx/users/model/user"
	"gitee.com/lzzandlyx/users/user"
	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type RegisterLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewRegisterLogic(ctx context.Context, svcCtx *svc.ServiceContext) *RegisterLogic {
	return &RegisterLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *RegisterLogic) Register(in *user.RegisterRequest) (*user.RegisterResponse, error) {
	if in.Info.LoginSource != "" {
	} else {
		if in.Info.Mobile == "" {
			return nil, fmt.Errorf("手机号不能为空")
		}
		if in.Info.Password == "" {
			return nil, fmt.Errorf("密码不能为空")
		}
		if in.Info.Salt == "" {
			return nil, fmt.Errorf("加盐不能为空")
		}
	}

	res, err := l.svcCtx.ModelUser.Register(l.ctx, &user1.User{
		Mobile:      in.Info.Mobile,
		Password:    in.Info.Password,
		Salt:        in.Info.Salt,
		Openid:      in.Info.Openid,
		Unionid:     in.Info.Unionid,
		LoginSource: in.Info.LoginSource,
	})
	if err != nil {
		return nil, err
	}

	var resInfo user.UserInfo
	err = copier.Copy(&resInfo, &res)
	if err != nil {
		return nil, err
	}

	return &user.RegisterResponse{
		Info: &resInfo,
	}, nil
}
