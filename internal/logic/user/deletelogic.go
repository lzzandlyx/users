package userlogic

import (
	"context"
	"fmt"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteLogic {
	return &DeleteLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteLogic) Delete(in *user.DeleteRequest) (*user.DeleteResponse, error) {
	if in.ID == 0 {
		return nil, fmt.Errorf("ID 不能为空")
	}

	err := l.svcCtx.ModelUser.Delete(l.ctx, in.ID)
	if err != nil {
		return nil, err
	}

	return &user.DeleteResponse{}, nil
}
