package userlogic

import (
	"context"
	"gitee.com/lzzandlyx/users/internal/svc"
	user1 "gitee.com/lzzandlyx/users/model/user"
	"gitee.com/lzzandlyx/users/user"
	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type CreatePointRecordLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreatePointRecordLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreatePointRecordLogic {
	return &CreatePointRecordLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreatePointRecordLogic) CreatePointRecord(in *user.CreatePointRecordRequest) (*user.CreatePointRecordResponse, error) {
	res, err := l.svcCtx.ModelPointRecord.CreatePointsRecord(l.ctx, &user1.PointRecord{
		UserId:      in.Info.UserId,
		ChangeType:  in.Info.ChangeType,
		Amount:      in.Info.Amount,
		Description: in.Info.Description,
		Source:      in.Info.Source,
		OrderID:     in.Info.OrderID,
	})
	if err != nil {
		return nil, err
	}

	var resInfo user.UserInfo
	err = copier.Copy(&resInfo, &res)
	if err != nil {
		return nil, err
	}

	return &user.CreatePointRecordResponse{
		Info: &resInfo,
	}, nil
}
