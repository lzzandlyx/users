package userlogic

import (
	"context"
	"fmt"
	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"
	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetInfoByMobileLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetInfoByMobileLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetInfoByMobileLogic {
	return &GetInfoByMobileLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetInfoByMobileLogic) GetInfoByMobile(in *user.GetInfoByMobileRequest) (*user.GetInfoByMobileResponse, error) {
	if in.Mobile == "" {
		return nil, fmt.Errorf("手机号不能为空")
	}

	info, err := l.svcCtx.ModelUser.GetUserInfoByMobile(l.ctx, in.Mobile)
	if err != nil {
		return nil, err
	}

	var resInfo user.UserInfo
	err = copier.Copy(&resInfo, &info)
	if err != nil {
		return nil, err
	}

	return &user.GetInfoByMobileResponse{
		Info: &resInfo,
	}, nil
}
