package userlogic

import (
	"context"
	"gitee.com/lzzandlyx/users/model/history"
	"github.com/jinzhu/copier"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type CreateHistoryLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateHistoryLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateHistoryLogic {
	return &CreateHistoryLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateHistoryLogic) CreateHistory(in *user.CreateHistoryRequest) (*user.CreateHistoryResponse, error) {
	res, err := l.svcCtx.ModelHistory.CreateHistory(l.ctx, &history.BrowseHistory{
		UserId:     in.Info.UserId,
		GoodsId:    in.Info.GoodsId,
		Title:      in.Info.Title,
		Price:      in.Info.Price,
		Cover:      in.Info.Cover,
		BrowseDate: in.Info.BrowseDate,
	})

	var resInfo user.HistoryInfo
	err = copier.Copy(&resInfo, res)
	if err != nil {
		return nil, err
	}

	return &user.CreateHistoryResponse{
		Info: &resInfo,
	}, nil
}
