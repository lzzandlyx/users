package userlogic

import (
	"context"
	"fmt"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdatePwdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdatePwdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdatePwdLogic {
	return &UpdatePwdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdatePwdLogic) UpdatePwd(in *user.UpdatePwdRequest) (*user.UpdatePwdResponse, error) {
	if in.Mobile == "" {
		return nil, fmt.Errorf("手机号不能为空")
	}
	if in.Password == "" {
		return nil, fmt.Errorf("密码不能为空")
	}
	
	err := l.svcCtx.ModelUser.UpdatePwd(l.ctx, in.Mobile, in.Password)
	if err != nil {
		return nil, err
	}
	return &user.UpdatePwdResponse{}, nil
}
