package addresslogic

import (
	"context"
	"fmt"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDefaultAddrLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateDefaultAddrLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateDefaultAddrLogic {
	return &UpdateDefaultAddrLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateDefaultAddrLogic) UpdateDefaultAddr(in *user.UpdateDefaultAddrRequest) (*user.UpdateDefaultAddrResponse, error) {
	if in.UserID == 0 {
		return nil, fmt.Errorf("用户ID 不能为空")
	}
	if in.ID == 0 {
		return nil, fmt.Errorf("ID 不能为空")
	}

	err := l.svcCtx.ModelAdd.UpdateDefaultAddress(l.ctx, in.UserID, in.ID)
	if err != nil {
		return nil, err
	}
	return &user.UpdateDefaultAddrResponse{}, nil
}
