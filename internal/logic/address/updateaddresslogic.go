package addresslogic

import (
	"context"
	"gitee.com/lzzandlyx/users/model/address"
	"github.com/jinzhu/copier"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateAddressLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateAddressLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateAddressLogic {
	return &UpdateAddressLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateAddressLogic) UpdateAddress(in *user.UpdateAddressRequest) (*user.UpdateAddressResponse, error) {
	err := verify(in.Info)
	if err != nil {
		return nil, err
	}

	addr := address.Addresses{}

	err = copier.Copy(&addr, &in.Info)
	if err != nil {
		return nil, err
	}

	err = l.svcCtx.ModelAdd.UpdateAddress(l.ctx, in.ID, &addr)
	if err != nil {
		return nil, err
	}

	return &user.UpdateAddressResponse{}, nil
}
