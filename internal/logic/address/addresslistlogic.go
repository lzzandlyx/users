package addresslogic

import (
	"context"
	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type AddressListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAddressListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddressListLogic {
	return &AddressListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AddressListLogic) AddressList(in *user.AddressListRequest) (*user.AddressListResponse, error) {
	list, err := l.svcCtx.ModelAdd.AddressList(l.ctx)
	if err != nil {
		return nil, err
	}

	var addrInfoList []*user.AddressInfo

	for _, addr := range list {
		addrInfo := &user.AddressInfo{
			ID:        int64(addr.ID),
			UserId:    addr.UserId,
			Name:      addr.Name,
			Mobile:    addr.Mobile,
			Province:  addr.Province,
			City:      addr.City,
			County:    addr.County,
			Township:  addr.Township,
			Address:   addr.Address,
			IsDefault: addr.IsDefault,
		}
		addrInfoList = append(addrInfoList, addrInfo)
	}

	return &user.AddressListResponse{
		Info: addrInfoList,
	}, nil
}
