package addresslogic

import (
	"context"
	"fmt"
	"github.com/jinzhu/copier"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetDefaultAddrLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDefaultAddrLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDefaultAddrLogic {
	return &GetDefaultAddrLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDefaultAddrLogic) GetDefaultAddr(in *user.GetDefaultAddrRequest) (*user.GetDefaultAddrResponse, error) {
	if in.UserID == 0 {
		return nil, fmt.Errorf("userID不能为空")
	}
	res, err := l.svcCtx.ModelAdd.GetDefaultAddr(l.ctx, in.UserID)
	if err != nil {
		return nil, err
	}

	var resInfo user.AddressInfo
	err = copier.Copy(&resInfo, res)
	if err != nil {
		return nil, err
	}

	return &user.GetDefaultAddrResponse{
		Info: &resInfo,
	}, nil
}
