package addresslogic

import (
	"context"
	"fmt"
	"github.com/jinzhu/copier"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetAddressInfoByIDLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetAddressInfoByIDLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetAddressInfoByIDLogic {
	return &GetAddressInfoByIDLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetAddressInfoByIDLogic) GetAddressInfoByID(in *user.GetAddressInfoByIDRequest) (*user.GetAddressInfoByIDResponse, error) {
	if in.ID == 0 {
		return nil, fmt.Errorf("ID 不能为空")
	}
	res, err := l.svcCtx.ModelAdd.GetAddressInfoByID(l.ctx, in.ID)
	if err != nil {
		return nil, err
	}

	var resInfo user.AddressInfo
	err = copier.Copy(&resInfo, res)
	if err != nil {
		return nil, err
	}

	return &user.GetAddressInfoByIDResponse{
		Info: &resInfo,
	}, nil
}
