package addresslogic

import (
	"context"
	"fmt"
	"gitee.com/lzzandlyx/users/model/address"
	"github.com/jinzhu/copier"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type CreateAddLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateAddLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateAddLogic {
	return &CreateAddLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateAddLogic) CreateAdd(in *user.CreateAddRequest) (*user.CreateAddPwdResponse, error) {
	err := verify(in.Info)
	if err != nil {
		return nil, err
	}

	res, err := l.svcCtx.ModelAdd.CreateAdd(l.ctx, &address.Addresses{
		UserId:    in.Info.UserId,
		Name:      in.Info.Name,
		Mobile:    in.Info.Mobile,
		Province:  in.Info.Province,
		City:      in.Info.City,
		County:    in.Info.County,
		Township:  in.Info.Township,
		Address:   in.Info.Address,
		IsDefault: in.Info.IsDefault,
	})

	var resInfo user.AddressInfo
	err = copier.Copy(&resInfo, res)
	if err != nil {
		return nil, err
	}

	return &user.CreateAddPwdResponse{
		Info: &resInfo,
	}, nil
}

func verify(in *user.AddressInfo) error {
	if in.Name == "" {
		return fmt.Errorf("姓名不能为空")
	}
	if in.Mobile == "" {
		return fmt.Errorf("手机号不能为空")
	}
	if in.Province == "" {
		return fmt.Errorf("省份不能为空")
	}
	if in.City == "" {
		return fmt.Errorf("城市不能为空")
	}
	if in.County == "" {
		return fmt.Errorf("县区不能为空")
	}
	if in.Township == "" {
		return fmt.Errorf("乡镇不能为空")
	}
	if in.Address == "" {
		return fmt.Errorf("详细地址不能为空")
	}

	return nil
}
