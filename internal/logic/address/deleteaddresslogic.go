package addresslogic

import (
	"context"
	"fmt"

	"gitee.com/lzzandlyx/users/internal/svc"
	"gitee.com/lzzandlyx/users/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteAddressLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteAddressLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteAddressLogic {
	return &DeleteAddressLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteAddressLogic) DeleteAddress(in *user.DeleteAddressRequest) (*user.DeleteResponse, error) {
	if in.ID == 0 {
		return nil, fmt.Errorf("ID不能为空")
	}
	err := l.svcCtx.ModelAdd.DeleteAddress(l.ctx, in.ID)
	if err != nil {
		return nil, err
	}

	return &user.DeleteResponse{}, nil
}
