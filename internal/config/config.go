package config

import (
	"flag"
	"github.com/zeromicro/go-zero/zrpc"
)

var ConfigFile = flag.String("f", "etc/user.yaml", "the config file")

type Config struct {
	zrpc.RpcServerConf
	Mysql Mysql
}

type Mysql struct {
	Host     string
	Port     int64
	User     string
	Password string
	DBName   string
}
