package svc

import (
	"gitee.com/lzzandlyx/users/internal/config"
	"gitee.com/lzzandlyx/users/model/address"
	"gitee.com/lzzandlyx/users/model/history"
	"gitee.com/lzzandlyx/users/model/user"
)

type ServiceContext struct {
	Config           config.Config
	ModelUser        *user.User
	ModelAdd         *address.Addresses
	ModelHistory     *history.BrowseHistory
	ModelPointRecord *user.PointRecord
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:           c,
		ModelUser:        user.NewUser(),
		ModelAdd:         address.NewAddress(),
		ModelHistory:     history.NewHistory(),
		ModelPointRecord: user.NewPointRecord(),
	}
}
